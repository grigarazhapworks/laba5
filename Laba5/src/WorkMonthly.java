public class WorkMonthly extends WorkerBase{
    private double monthlyPay;

    @Override
    public int getId() {
        return super.id;
    }

    @Override
    public void setId(int id) {
        super.id = id;
    }

    @Override
    public void setName(String str) {
        super.name = str;
    }
    public WorkMonthly(int id ,String name, double monthlypay){
        this.id = id;
        this.name = name;
        this.monthlyPay=monthlypay;
    }
    @Override
    public String getName() {
        return super.name;
    }

    public void setMonthlyPay(double monthlyPay){
        this.monthlyPay = monthlyPay;
    }
    public double getMonthlyPay(){
        return this.monthlyPay;
    }
    @Override
    public double avMonthlyPay() {
        return monthlyPay;
    }

    @Override
    public String toString() {
        return String.format("№:%d ФИО:%s Среднемесячная зарплата %f",id,name,avMonthlyPay());
    }
}
