import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Количество сотрудников");
        int N = sc.nextInt();
        byte type;
        List<WorkerBase> a = new ArrayList<>();
        for(int i = 0;i<N;i++){
            System.out.println("Тип оплаты сотрудника: 1-почасовая оплата, 2 - месячная оплата");
            type = sc.nextByte();
            switch(type){
                case 1:
                    a.add(new WorkHourly(i+1,typeValue("Имя сотрудника: "),Double.parseDouble(typeValue("Часовая зарплата сотрудника: "))));
                    break;
                case 2:
                    a.add(new WorkMonthly(i+1,typeValue("Имя сотрудника: "),Double.parseDouble(typeValue("Месячная зарплата сотрудника: "))));
                    break;
            }
        }
        for(int i = 0;i<a.size()-1;i++){
            for(int j=i+1;j<a.size();j++){
                if(a.get(i).avMonthlyPay()<a.get(j).avMonthlyPay()){
                    WorkerBase temp = a.get(i);
                    a.set(i,a.get(j));
                    a.set(j,temp);
                }
            }
        }
        System.out.println("Сотрудники: ");
        for(int i = 0;i<a.size();i++){
            System.out.println(a.get(i).toString());
        }
        System.out.println("Первые 5 сотрудников");
        for(int i = 0;i<5;i++){
            System.out.println(a.get(i).toString());
        }
        System.out.println("Последние 3 сотрудника");
        for(int i = a.size()-3;i<a.size();i++){
            System.out.println(a.get(i).toString());
        }

    }public static String typeValue(String type){
        Scanner scan = new Scanner(System.in);
        System.out.println(type);
        return scan.next();
    }
}
