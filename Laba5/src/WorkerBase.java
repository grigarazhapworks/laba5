public abstract class WorkerBase {
    public int id;
    public String name;
    public abstract int getId();
    public abstract String getName();
    public abstract void setId(int id);
    public abstract void setName(String str);
    public abstract double avMonthlyPay();
}
