public  class WorkHourly extends WorkerBase {
    private double stavkaVChas;
    @Override
    public void setId(int id) {
        super.id = id;
    }

    @Override
    public int getId() {
        return super.id;
    }
    public WorkHourly(int id, String name, double stavkaVChas){
        this.id = id;
        this.name = name;
        this.stavkaVChas = stavkaVChas;
    }
    @Override
    public String getName() {
        return super.name;
    }
    public double getStavkaVChas(){
        return this.getStavkaVChas();
    }
    public void setStavkaVChas(double svc){
        this.stavkaVChas = svc;
    }
    @Override
    public void setName(String str) {
        super.name = name;
    }
    @Override
    public double avMonthlyPay() {
        return 20.8*8*stavkaVChas;
    }
    @Override
    public String toString() {
        return String.format("№:%d ФИО:%s Среднемесячная зарплата %f",id,name,avMonthlyPay());
    }
}
